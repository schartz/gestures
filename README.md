# Gestures
A minimal Gtk+ GUI app for libinput-gestures

## Dependencies:
- Python 3 with `gi` module
- python3-setuptools
- xdotool (recommended)
- libinput-gestures
- libinput-tools

On Debian/Ubuntu, type:
`sudo apt install python3 python3-setuptools xdotool python3-gi libinput-tools python-gobject`

To install libinput-gestures, follow the instructions on its official page: https://github.com/bulletmark/libinput-gestures


## Install
Please install the above dependencies before proceeding.
`git clone https://gitlab.com/cunidev/gestures`
`cd gestures`
`sudo python3 setup.py install`

Solus users can install Gestures directly from the official repos:
`sudo eopkg it gestures`


## Video demo:

<a href="http://www.youtube.com/watch?feature=player_embedded&v=MrOIEoyijXM
" target="_blank"><img src="http://img.youtube.com/vi/MrOIEoyijXM/0.jpg" 
alt="(click to open video)" width="480" height="360" border="10" /></a>
